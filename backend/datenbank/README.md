# Database
For this projekt database is a part of its backend to provide data for its frontend.
This data will contain a database ligth_walk and its schema lightwalk on a postgres db.
It will be used to cover geometry on a certain location and with a given id.

# Technolgie Stack
PostGres SQL Database is used as data storage.
It will run in an Container. You'll need podman or docker and run docker-compose or podman-compose.

# Sources
You will find a docker compose file as well as a sql script with a provided data for zurich.
On zurich you'll receive lamping spots. Please regard that this data is received by public sources.

# Getting startet
Run docker-compose file.

```bash
podman-compose up --remove-orphans
```
~~Run provided sql file against database with its credential provided by docker-compose-file.~~
Run python import file to receive data from given csv file.

# On Shutdown
If you would like to shut down your database
```bash
podman-compose down --volume
```


# Authors
Peter 
Daniel

