CREATE SCHEMA IF NOT EXISTS lightwalk
    AUTHORIZATION pr0pm;

-- Table: lightwalk.geometry

-- DROP TABLE IF EXISTS lightwalk.geometry;

CREATE TABLE IF NOT EXISTS lightwalk.geometry
(
    tag text COLLATE pg_catalog."default",
    geojson jsonb
)

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS lightwalk.geometry
    OWNER to pr0pm;

GRANT ALL ON TABLE lightwalk.geometry TO pr0pm;