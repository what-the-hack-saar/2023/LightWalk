# Read Me First
The following was discovered as part of building this project:

* The original package name 'what-the-hack.saarland.lightwalk' is invalid and this project uses 'whatthehack.saarland.lightwalk' instead.

# Getting Started

This project uses kotlin and spring-boot-3 and based on Java 17 SDK

Before you start the project you'll need to run:
```bash
mvn clean install
```
To start the Project use this command:
```bash
mvn spring-boot:run
```

# Endpoints
| Url                                           | Description |
|-----------------------------------------------|-------------|
| http://localhost:8080/v3/api-docs/swagger-ui/ | Swagger-UI  |
| http://localhost:8080/api/v1/geometry"        | $80         |


### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/3.1.4/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/3.1.4/maven-plugin/reference/html/#build-image)

# Architecture

# Contributor
Daniel Rhein daniel.rhein84@googlemail.com

# Licence
MIT LICENCE