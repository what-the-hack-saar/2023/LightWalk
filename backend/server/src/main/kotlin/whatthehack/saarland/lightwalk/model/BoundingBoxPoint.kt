package whatthehack.saarland.lightwalk.model

public data class BoundingBoxPoint (
    val lat:Double=0.0,
    val lng:Double=0.0
)