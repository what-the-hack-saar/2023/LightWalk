package whatthehack.saarland.lightwalk.controller

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import whatthehack.saarland.lightwalk.model.BoundingBox
import whatthehack.saarland.lightwalk.model.Geometry
import whatthehack.saarland.lightwalk.repository.GeometryService

@RequestMapping("api/v1/geometry")
@RestController
class GeometryController(val service: GeometryService) {

    @GetMapping
    fun getAllGeometry() = service.getAll()

    @PostMapping("bbx")
    fun getGeometryWithinBoundingBox(@RequestBody boundingBox: BoundingBox) = service.findWithinBoundingBox(boundingBox)

    
    @GetMapping("/{id}")
    fun getGeometry(@PathVariable id: Long) = service.getById(id)

    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun saveGeometry(@RequestBody geometry: Geometry): Geometry = service.create(geometry)

    
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteGeometry(@PathVariable id: Long) = service.remove(id)

   /* @PutMapping("/{id}")
    fun updatePlayer(
            @PathVariable id: String, @RequestBody geometry: Geometry
    ) = service.update(id, geometry)*/
}