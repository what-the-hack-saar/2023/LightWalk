package whatthehack.saarland.lightwalk.repository

import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import whatthehack.saarland.lightwalk.model.BoundingBox
import whatthehack.saarland.lightwalk.model.Geometry

@Service
class GeometryService(val repository: GeometryRepository) {
    fun getAll(): List<Geometry> = repository.findAll()

    fun getById(id: Long): Geometry = repository.findByIdOrNull(id) ?:
    throw ResponseStatusException(HttpStatus.NOT_FOUND)

    fun findWithinBoundingBox(boundingBox:BoundingBox):List<Geometry> = repository.getListBetweenBoundingBox(boundingBox.pt1.lat,boundingBox.pt1.lng,boundingBox.pt2.lat,boundingBox.pt2.lng)

    fun create(geometry: Geometry): Geometry = repository.save(geometry)

    fun remove(id: Long) {
        if (repository.existsById(id)) repository.deleteById(id)
        else throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    fun update(id: Long, geometry: Geometry): Geometry {
        return if (repository.existsById(id)) {
            geometry.id = id
            repository.save(geometry)
        } else throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }
}