package whatthehack.saarland.lightwalk.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import whatthehack.saarland.lightwalk.model.BoundingBox
import whatthehack.saarland.lightwalk.model.Geometry

@Repository
interface GeometryRepository : JpaRepository<Geometry, Long>
{
    /*
    = await sql` SELECT geojson FROM lightwalk.geometry
    where lat between ${requestModel.pt2.lat} and ${requestModel.pt1.lat}
    and
    long between  ${requestModel.pt1.lng} and ${requestModel.pt2.lng}`;
     */
    @Query("FROM Geometry where lat between :p2x and  :p1x and my_long between :p1y and :p2y")
    fun getListBetweenBoundingBox(@Param("p1x") x1:Double,@Param("p1y")y1:Double,@Param("p2x")x2:Double,@Param("p2y")y2:Double):List<Geometry>
}