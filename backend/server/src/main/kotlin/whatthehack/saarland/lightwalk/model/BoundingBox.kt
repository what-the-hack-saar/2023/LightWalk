package whatthehack.saarland.lightwalk.model

public data class BoundingBox (
    public val pt1:BoundingBoxPoint,
    public val pt2:BoundingBoxPoint
)