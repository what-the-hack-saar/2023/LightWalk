package whatthehack.saarland.lightwalk.model


import jakarta.persistence.*
import org.springframework.data.annotation.Id

@Entity
@Table(name = "geometry", schema="lightwalk")
public data class Geometry (
        @jakarta.persistence.Id @Id @GeneratedValue(strategy = GenerationType.AUTO) public var id:Long=0,
        public val geojson:String = "",
        public val lat:Double = 0.0,
        @Column(name="long")
        public val my_long:Double=0.0
)
{

}