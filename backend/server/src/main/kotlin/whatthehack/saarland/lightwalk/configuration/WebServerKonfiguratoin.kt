package whatthehack.saarland.lightwalk.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

import org.springframework.beans.factory.annotation.Value

@Configuration
class WebServerKonfiguratoin {


    @Value("\${cors.originPatterns:default}")
    private val corsOriginPatterns: String = ""


    @Bean
    fun addCorsConfig(): WebMvcConfigurer {
        return object : WebMvcConfigurer {
            override fun addCorsMappings(registry: CorsRegistry) {

                val allowedOrigins = corsOriginPatterns.split(",").toTypedArray()
                registry.addMapping("/**")
                        .allowedMethods("*")
                        .allowedOriginPatterns(*allowedOrigins)
                        .allowCredentials(true)
            }
        }
    }
}