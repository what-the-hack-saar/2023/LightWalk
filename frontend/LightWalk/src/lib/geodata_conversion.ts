import type { LatLngBounds } from 'leaflet';
import type { RequestModel } from '../types/requestTypes';

const latLngBoundsToRequestModel = (bounds: LatLngBounds): RequestModel => {
	const northWestPoint = bounds.getNorthWest();
	const southEastPoint = bounds.getSouthEast();
	const requestModel: RequestModel = {
		pt1: {
			lat: northWestPoint.lat,
			lng: northWestPoint.lng
		},
		pt2: {
			lat: southEastPoint.lat,
			lng: southEastPoint.lng
		}
	};
	return requestModel;
};

export { latLngBoundsToRequestModel };
