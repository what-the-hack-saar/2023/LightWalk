export type RequestModel = {
	pt1: RequestBBoxPoint;
	pt2: RequestBBoxPoint;
};
export type RequestBBoxPoint = {
	lat: number;
	lng: number;
};
