import { decodePolyline } from '$lib/polylineDecoder';
import Openrouteservice from 'openrouteservice-js';
import { OPENROUTESERVICE_API_KEY } from '$env/static/private';
import fs from 'fs';
import data from '$lib/exports/request.json';

export async function POST() {
	const value = false;
	let result;
	const filePath = './src/lib/exports/request.json'; // Replace with your desired file path
	if (value) {
		console.log('asking openroutservice');
		const orsDirections = new Openrouteservice.Directions({
			api_key: OPENROUTESERVICE_API_KEY
		});
		result = await orsDirections.calculate({
			coordinates: [
				[8.501358032226564, 47.382311560261506],
				[8.525047302246096, 47.3746396594011]
			],
			profile: 'foot-walking',
			extra_info: ['waytype', 'steepness'],
			format: 'json',
			api_version: 'v2',
			alternative_routes: { target_count: 3 }
		});

		const response = JSON.stringify(result, null, '\t');
		fs.writeFile(filePath, response, (err) => {
			err ? console.log(err) : console.log('allfine');
		});
	} else {
		console.log('not asking openroutservice; reading file');
		//const t = await import ('$lib/exports/request.json')
		result = data;
	}

	// Write the JSON data to the file

	let polylineLatLngList: number[][][] | undefined = undefined;
	result.routes.forEach((route: object) => {
		const geom = route.geometry;
		//polylineLatLngList.append(decodePolyline(geom, false));
		if (polylineLatLngList === undefined) {
			polylineLatLngList= [decodePolyline(geom, false)]
		} else {
			polylineLatLngList.push(decodePolyline(geom, false));
		}
	});
	return new Response(JSON.stringify(polylineLatLngList));
}
