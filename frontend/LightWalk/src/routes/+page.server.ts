	export const ssr = false;
export async function load({ locals }) {
	const { sql } = locals;

	const result = await sql`SELECT geojson FROM lightwalk.geometry limit 500`;
	const answer: GeoJSON.Feature[] = result.map((e) => {
		return e.geojson as GeoJSON.Feature;
	});
	const geojsonFeatureCollection: GeoJSON.FeatureCollection = {
		features: answer,
		type: 'FeatureCollection'
	};
	return { result: geojsonFeatureCollection };
}
