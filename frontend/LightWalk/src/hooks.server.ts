
import postgres from 'postgres';

export const handle = async ({event, resolve}) => {
  const sql = postgres('postgres://pr0pm:wooph8Ze12@localhost:5432/light_walk');

  event.locals = {
    sql: sql
  };
  const response = await resolve(event);
  return response;
};