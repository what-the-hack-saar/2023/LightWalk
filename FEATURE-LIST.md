# FEATURES

We appreciate to achieve all features as soon as possible.
In its current state our project won't fullfilled all features

| Legende | Beschreibung    |
|---------|-----------------|
| OP      | OFFEN           |
| WIP     | WORK IN PROGRESS |
| D    | ERLEDIGT        |
| WTD     | WONT DO|

## FRONTEND

| Feature                                                       | Status |
|---------------------------------------------------------------|--------|
| Anzeige von Laternen mit Testdaten                            | D      |
| Route anzeigen                                                | D      |
| Route start ende festlegen                                    | OP     |
| Mehrere Routen anzeigen                                       | D      |
| Eigene Laternen anzeigen                                      | OP     |
| Testdaten bereitstellen fuer Routenermittlung ueber backend   | OP     |
| Crossfunktionale bereitstellung als Native App in iOS/Android | OP     |
| Dokumentation der Frontendschnittstelle                       | OP     |

##BACKEND

| Feature                                                             | Status |
|---------------------------------------------------------------------|--------|
| Datentabelle importieren in PostgresSQL importieren                 |D|
| DB Daten in PostgresSQL importieren                                 |D|
| Restschnittstelle zum erstellen von Laterneninformationen           |D|
| Restschnittstelle zum loeschen von Laterneninformationen            |D|
| Restschnittstelle auslesen von Laterneninformationen                |D|
| Restschnittstelle auslesen im BoundingBox von Laterneninformationen |D|
| Dokumentation der Backendschnittstelle                              |TBD|

## PROJECT

| Feature                                               | Status |
|-------------------------------------------------------|--------|
| Changelog erstellen                                   |D|
| Readme fertigstelle                                   |D|
| Insom-Beispiele in Insomnia_2023-10-15 bereitgestellt |D|

