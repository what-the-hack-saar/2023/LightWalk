# ChangeLog

````
git log --pretty=format:"|%h | %an| %ah | %s |"
````

| com_ssh  | Author           | Date            | Message                                                                                                                          |   |
|----------|--------------|-----------------|----------------------------------------------------------------------------------------------------------------------------------|---|
|1535c23 | Peter S| 4 minutes ago | Merge branch 'develop-frontend' into 'develop' |
|896eebf | peterseubert| 7 minutes ago | fix: disabeling openroutservice requests |
|d00a5da | Peter S| 52 minutes ago | Merge branch 'develop-backend' into 'develop' |
|60e5e09 | Peter S| 62 minutes ago | Update +server.ts |
|2ecb7f9 | peterseubert| 64 minutes ago | fix: rename of polylineDecoder |
|b9365fd | danielRhein| 8 hours ago | Insomnia-Test-Jason added. |
|e84fc3c | danielRhein| 8 hours ago | Added Documentation, Json-Information and changed Application-Properties for swagger-url. |
|d2b08ea | danielRhein| 9 hours ago | Update Help.md |
|6f9bc70 | danielRhein| 9 hours ago | create find within bounding box and stabilized application. |
|8424173 | peterseubert| 10 hours ago | wip: navigation example |
|48c1fe8 | danielRhein| Sat 23:20 | Fix: improve database.sql |
|75af234 | danielRhein| Sat 23:19 | Added initalisation to docker-compose.yml |
|453a981 | peterseubert| Sat 23:05 | feat: finished display of points |
|927b6a7 | danielRhein| Sat 22:38 | Added podman-up information |
|7176263 | danielRhein| Sat 22:28 | added Readme for database. Please provide your E-Mail-Address an full name of its contributors. And Improve Getting-Startet part |
|767e8fb | danielRhein| Sat 22:10 | Update README.md |
|4c9463a | danielRhein| Sat 21:56 | Merge branch 'develop-backend' of gitlab.com:what-the-hack-saar/2023/LightWalk into develop-backend |
|31c5836 | danielRhein| Sat 21:55 | Zwischenstand. Arbeiten an Geometry and its service. |
|9edf690 | peterseubert| Sat 21:54 | wip: select on query data |
|c828700 | peterseubert| Sat 20:10 | fix: changed db schema |
|00e112d | peterseubert| Sat 17:27 | feat: display of basic lanterns |
|df5030f | Peter S| Sat 13:48 +0000 | Merge branch 'develop-database-rework' into 'develop-backend' |
|8c8e8d8 | peterseubert| Sat 15:47 | feat: using json data |
|4e4c373 | peterseubert| Sat 15:14 | wip |
|2cf009b | Daniel Rhein| Sat 14:35 | Improve application properties geometry and its service |
|be801d3 | Daniel Rhein| Sat 14:34 | Improve application properties geometry and its service |
|24900e7 | NF117| Sat 12:15 +0000 | Merge branch 'backend-database-import' into 'develop-backend' |
|67407a6 | peterseubert| Sat 14:10 | fix: working import |
|28d9300 | Daniel Rhein| Sat 14:04 | ADDED SIMPLE SERVER |
|eee76b8 | Daniel Rhein| Sat 13:40 | Merge branch 'develop-backend' of gitlab.com:what-the-hack-saar/2023/LightWalk into develop-backend |
|f246054 | peterseubert| Sat 13:34 | feat: database creation script and import script |
|7fa42ea | Daniel Rhein| Sat 13:19 | Added Database.sql and inital projekt informationen |
|06eebb3 | tobias| Sat 13:17 | Datenbank docker-compose.yml |
|a41971c | tobias| Sat 13:15 | Datenbank docker-compose.yml |
|7ffa080 | Daniel Rhein| Sat 13:13 | Added Database.sql |
|80ff8a9 | peterseubert| Sat 12:58 | feat: leaflet set to zuerich |
|9fceaf8 | Daniel Rhein| Sat 12:27 | Added inital server |
|ccfef3d | peterseubert| Sat 12:13 | feat: leaflet types added |
|b263807 | peterseubert| Sat 11:48 | leaflet init |
|b5a8ba8 | peterseubert| Sat 11:44 | feat: setup of sveltekit |
|06e7321 | peterseubert| Sat 11:09 | test |
|2a7b941 | Daniel Rhein| Sat 11:08 | Added OsmStreetlight-Vorlage |
|d95bae6 | Pascal Stein| Sat 08:22 +0000 | Initial commit |