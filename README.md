# LightWalk

Providing a path through darkness is possible on illuminated ways. 
Navigating through darkness is the mission of ligthwalk-app.   

## Status
![example workflow](https://gitlab.com/what-the-hack-saar/2023/LightWalk.git/badge.svg)

## Visionery
Dr. Christoph Endres

Und hier noch der Link zur ursprünglichen Idee: 

|  https://x.com/kattascha/status/1537511718095659008  |
|:----|
|xJedes Navi: „Nachts um 3 als Frau allein zu Fuß 0,5km durch den unbeleuchteten Park - sure, why not?!“ - ernsthaft, warum kann man solche Vorschläge in den Einstellungen nicht ausschließen? 🙄|
|Twitter•16.06.2022 21:05|


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Directory Description
| Folder | Description                        |
|--------|------------------------------------|
|backend| contains database and rest-service |
|frontend| contains web-application           |

## Technologie stack
| Folder               | Description                                             |
|----------------------|---------------------------------------------------------|
| backend/database     | postgresSql within docker-Container or Podmancontainer  |
| backend/server       | Kotlin with Java 17 and Spring-Boot3 and Maven-Buildset |
| backend/frontend     | Npm-Build with svelteKit, https://openrouteservice.org/                              |
| osmstreetlight-clone | Example for OSM-Requests not a part of our project      |

## Collaborate with your team
Feel free to contribute or fork our project. 

## Roadmap
You'll find our feature list in FEATURE-LIST.md

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

| Author        | Email                         | Contributed                                             |
|:--------------|:------------------------------|:--------------------------------------------------------|
| Peter Seubert | ---                           | Enlighted every back and frontend-parts in this project |
| Daniel Rhein  | daniel.rhein84@googlemail.com | Work in Team as Code-Monkey                             |

## License
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
![example workflow](https://gitlab.com/what-the-hack-saar/2023/LightWalk/-/tree/develop/workflows/main.yml/badge.svg?branch=develop)